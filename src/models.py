from django.db import models


class Tables(models.Model):
    rectangular = 0
    oval = 1
    TYPES_OF_SHAPE = [
        (rectangular, "rectangular"),
        (oval, "oval"),
    ]
    number = models.IntegerField(unique=True)
    seats = models.IntegerField()
    shape = models.IntegerField(choices=TYPES_OF_SHAPE)
    latitude = models.DecimalField(max_digits=5, decimal_places=2)
    longitude = models.DecimalField(max_digits=5, decimal_places=2)
    length = models.DecimalField(max_digits=4, decimal_places=2)
    width = models.DecimalField(max_digits=4, decimal_places=2)

    def __str__(self):
        return '%s' % str(self.number)


class Schema(models.Model):
    tables = models.ManyToManyField(Tables, related_name="tables")


class Order(models.Model):
    date = models.DateField()
    name = models.CharField(null=True, max_length=255)
    email = models.EmailField()
    table = models.ManyToManyField(Tables, related_name="order_table")






