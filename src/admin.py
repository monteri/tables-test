from django.contrib import admin
from .models import Tables, Schema, Order

admin.site.register(Tables)
admin.site.register(Schema)
admin.site.register(Order)
