from django import forms
from src.models import Tables, Order


class OrderForm(forms.Form):
    table = forms.ModelMultipleChoiceField(queryset=Tables.objects.all(),
                widget=forms.CheckboxSelectMultiple(attrs=
                                    {
                                        'type': "checkbox",
                                        'class': "checkbox",
                                        'onclick': "myFunction()",
                                    }),
    )
    email = forms.EmailField(label='Email', widget=forms.EmailInput(attrs=
                                                     {
                                                         'class': 'form-control col-sm-6 mb-2',
                                                         'id': 'email',
                                                         'style': 'display:none',
                                                         'placeholder': 'example@mail.com'
                                                     }))
    name = forms.CharField(label='Name', widget=forms.TextInput(attrs=
                                                     {
                                                         'class': 'form-control col-sm-6 mb-2',
                                                         'id': 'name',
                                                         'style': 'display:none',
                                                         'placeholder': 'Your name'
                                                     }))
    class Meta:
        model = Order
        exclude = ('date')