from django.shortcuts import render, HttpResponse
from django.views.generic import FormView, TemplateView
from django.shortcuts import render
from django.core.mail import send_mail
from datetime import datetime, timedelta

from src.models import Order, Schema, Tables
from src.forms import OrderForm


class DateFormView(TemplateView):
    template_name = "src/form.html"


class SchemaView(FormView):
    template_name = "src/form_detail.html"

    def get(self, request, *args, **kwargs):
        query = self.request.GET.get('q')
        orders = Order.objects.filter(date=query)
        schema = Schema.objects.last()
        day_before = datetime.strptime(query, "%Y-%m-%d").date() - timedelta(days=1)
        day_after = datetime.strptime(query, "%Y-%m-%d").date() + timedelta(days=1)
        form = OrderForm()
        context = {
            'current_day': query,
            'day_before': day_before,
            'day_after': day_after,
            'orders': orders,
            'schema': schema,
            'form': form,
        }
        return render(self.request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = OrderForm(request.POST or None)
        if request.method == "POST" and form.is_valid():
            tables = form.cleaned_data['table']
            email = form.cleaned_data['email']
            name = form.cleaned_data['name']
            date = self.request.GET.get('q')
            order = Order.objects.create(date=date, email=email, name=name)
            order.table.set(tables)
            order.save()
            table_str = '{'
            for qs in tables:
                table_str = table_str + str(qs) + ' '
            table_str = table_str + '}'
            send_mail('Hello ' + name,
                      'This is an automated message. You booked tables №' + table_str + ' for ' + date,
                      'lansevermore@gmail.com',
                      {email},
                      fail_silently=False)
            return render(request, 'src/success_email.html')
        return HttpResponse("Something went wrong")





