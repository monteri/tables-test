from django.urls import path
from src import views

urlpatterns = [
    path('', views.DateFormView.as_view()),
    path('order', views.SchemaView.as_view(), name="schema_view")
]